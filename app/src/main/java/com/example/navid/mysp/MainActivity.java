package com.example.navid.mysp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText edtName;
    EditText edtFamily;
    EditText edtPhoneNumber;
    TextView txtName;
    TextView txtFamily;
    TextView txtPhoneNumber;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtName = (EditText) findViewById(R.id.edtName);
        edtFamily = (EditText) findViewById(R.id.edtFamily);
        edtPhoneNumber = (EditText) findViewById(R.id.edtPhoneNumber);
        txtName = (TextView) findViewById(R.id.txtName);
        txtFamily = (TextView) findViewById(R.id.txtFamily);
        txtPhoneNumber = (TextView) findViewById(R.id.txtPhoneNumber);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {


            public void onClick(View v) {
            txtName.setText(edtName.getText());
            txtFamily.setText(edtFamily.getText());
            txtPhoneNumber.setText(edtPhoneNumber.getText());
            }
        });

    }


}
